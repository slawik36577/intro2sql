-- create a schema/table for the following processing
-- MVS is not providing proper JSON format data, it's reason to load data into "text" column.
CREATE SCHEMA IF NOT EXISTS stage;
CREATE TABLE IF NOT EXISTS stage.raw_texts (raw_text text);

/*
Loading data to DB from the loaded file "mvswantedmt.json"
The file was loaded with curl application via defined script in .gitlab-ci.yml

\COPY command means that we will copy the file from the local container
COPY (without the slash) will try to look for the file on our DB server.
https://stackoverflow.com/questions/51681905/difference-between-copy-and-copy-commands-in-postgresql
*/
\COPY stage.raw_texts FROM mvswantedpassport_z.json;

-- Verify loading to stage.raw_texts
SELECT rt.raw_text
FROM stage.raw_texts rt
LIMIT 5;

-- As MVS provides incorrect jsons we should verify strings
-- We can try converting (text->json) with the following function.
CREATE OR REPLACE FUNCTION try_cast_json(p_in TEXT, p_default JSON DEFAULT NULL)
   RETURNS JSON
AS
$$
BEGIN
  BEGIN
    RETURN $1::JSON;
  EXCEPTION
    WHEN OTHERS THEN
       RETURN '{"error": 1}';
  END;
END;
$$
LANGUAGE plpgsql;

-- Create table with json column
CREATE TABLE IF NOT EXISTS stage.raw_jsons (raw_json json);

-- Trim text string, without coma.
INSERT INTO stage.raw_jsons
SELECT try_cast_json(LEFT(rt.raw_text, LENGTH(rt.raw_text)-1)) AS raw_json
FROM stage.raw_texts rt
WHERE try_cast_json(LEFT(rt.raw_text, LENGTH(rt.raw_text)-1))::text <> '{"error": 1}';

-- Verify loading to stage.raw_jsons
SELECT rj.raw_json
FROM stage.raw_jsons rj
LIMIT 5;

-- Create a temporary table (into "stage" schema to reload data)
-- DROP TABLE stage.claims;
CREATE TABLE IF NOT EXISTS stage.claims
(
    id BIGINT
  , insert_date TIMESTAMP
  , D_TYPE CHARACTER VARYING(50)
  , D_NUMBER CHARACTER VARYING(50)
  , D_SERIES CHARACTER VARYING(15)
  , ovd CHARACTER VARYING(200)
  , D_STATUS CHARACTER VARYING(50)
  , THEFT_DATA TIMESTAMP
);

-- Convert to columns
INSERT INTO stage.claims
SELECT (rj.raw_json->>'ID')::BIGINT AS id
  , (rj.raw_json->>'INSERT_DATE')::TIMESTAMP AS insert_date
  , (rj.raw_json->>'D_STATUS')::CHARACTER VARYING(50) AS DST
  , (rj.raw_json->>'D_NUMBER')::CHARACTER VARYING(50) AS DN
  , (rj.raw_json->>'D_SERIES')::CHARACTER VARYING(15) AS DSE
  , (rj.raw_json->>'OVD')::CHARACTER VARYING(200) AS ovd
  , (rj.raw_json->>'D_TYPE')::CHARACTER VARYING(50) AS DT
  , (rj.raw_json->>'THEFT_DATA')::TIMESTAMP AS TD
FROM stage.raw_jsons rj;

-- Verify processing
SELECT c.id
, c.insert_date
, c.DST
, c.DN
, c.DSE
, c.ovd
, c.DT
, c.TD
FROM stage.claims c
LIMIT 5;

-- Create persistent table
CREATE TABLE IF NOT EXISTS public.claims
(
    id BIGINT
  , insert_date TIMESTAMP
  , D_STATUS CHARACTER VARYING(50)
  , D_NUMBER CHARACTER VARYING(50)
  , D_SERIES CHARACTER VARYING(15)
  , ovd CHARACTER VARYING(200)
  , D_TYPE CHARACTER VARYING(50)
  , THEFT_DATA TIMESTAMP
);

-- Speed up search with B-Tree index
/*CREATE INDEX IF NOT EXISTS idx_claims_imei ON public.claims (imei);*/

-- Add new claims
INSERT INTO public.claims
SELECT t.id
  , t.insert_date
  , t.D_STATUS
  , t.D_NUMBER
  , t.D_SERIES
  , t.ovd
  , t.D_TYPE
  , t.THEFT_DATA
FROM stage.claims t
WHERE NOT EXISTS (SELECT NULL
                  FROM public.claims c
                  WHERE t.D_SERIES = c.D_SERIES)
      AND t.D_SERIES IS NOT NULL;

-- Drop temporary objects
DROP SCHEMA stage CASCADE;
